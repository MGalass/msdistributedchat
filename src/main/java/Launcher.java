import verticles.DatabaseVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import verticles.GlobalRegistryVerticle;

public class Launcher extends AbstractVerticle {

	public static void main(String[] args) {
		final Vertx vertx = Vertx.vertx();

		/* DatabaseVerticle */
		DatabaseVerticle databaseVerticle = new DatabaseVerticle("dc", "root", "");
		vertx.deployVerticle(databaseVerticle, res -> {
			if (res.succeeded()) {
				System.out.println("[Launcher] DatabaseVerticle deployed");
                vertx.deployVerticle(new GlobalRegistryVerticle(databaseVerticle), r -> {
                    System.out.println(r.succeeded() ?
                            "[Launcher] GlobalRegistryVerticle deployed" :
                            "[Launcher] Error: GlobalRegistryVerticle deployment failed: " + r.cause().getLocalizedMessage());
                });
			} else {
				System.out.println("[Launcher] Error: DatabaseVerticle deployment failed: " + res.cause().getLocalizedMessage());
			}
		});
	}
}
