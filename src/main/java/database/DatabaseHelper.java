package database;

import utils.GenericResponse;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.ResultSet;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;
import rx.functions.Action1;

import java.util.List;
import java.util.Optional;

abstract class DatabaseHelper {

    private final SQLClient client;

    DatabaseHelper(final SQLClient client) {
        this.client = client;
    }

    /**
     * This method send a not prepared query to the client requested database
     * @param query the not prepared query
     * @param onResult an action containing true if the query succeeded, false otherwise
     */
    void executeQuery(String query, Action1<GenericResponse> onResult) {
        executeConnection(connResult -> {
            if (connResult.isPresent()) {
                SQLConnection connection = connResult.get();
                connection.query(query, queryResult -> {
                    if (queryResult.succeeded()) {
                        /* If the query succeeded */
                        onResult.call(new GenericResponse(true, null));
                    } else {
                        /* If the query failed */
                        onResult.call(new GenericResponse(false, queryResult.cause().getLocalizedMessage()));
                    }

                    /* Close connection */
                    connection.close();
                });
            } else {
                log("Connection to the database failed");
                onResult.call(new GenericResponse(false, "Connection to the database failed"));
            }
        });
    }

    /**
     * This method send a not prepared query to the client requested database
     * @param query the not prepared query
     * @param onSuccess an action called on success containing the query result
     * @param onFailure an action called on failure containing the error message
     */
    void executeQuery(String query, Action1<JsonArray> onSuccess, Action1<String> onFailure) {
        executeConnection(connResult -> {
            if (connResult.isPresent()) {
                SQLConnection connection = connResult.get();
                connection.query(query, queryResult -> {
                    if (queryResult.succeeded()) {
                        /* If the query succeeded */
                        ResultSet rs = queryResult.result();
                        /* It may return null even if succeeded (on insert for instance) */
                        if (rs != null) {
                            JsonArray jsonArray = new JsonArray();
                            for (int i = 0; i < rs.getResults().size(); i++) {
                                /* Creating a json for every result */
                                List arrayList = rs.getResults().get(i).getList();
                                /* Parsing the fields */
                                JsonObject temp = new JsonObject();
                                for (int j = 0; j < arrayList.size(); j++) {
                                    temp.put(rs.getColumnNames().get(j),arrayList.get(j));
                                }
                                jsonArray.add(temp);
                            }
                            onSuccess.call(jsonArray);
                        } else {
                            onSuccess.call(new JsonArray());
                        }
                    } else {
                        /* If the query failed */
                        onFailure.call(queryResult.cause().getLocalizedMessage());
                    }

                    /* Close connection */
                    connection.close();
                });
            } else {
                onFailure.call("Connection to the database failed");
            }
        });
    }

    /**
     * This method execute a connection to the client database
     * @param onResult an action containing the connection (as an Optional)
     */
    private void executeConnection(Action1<Optional<SQLConnection>> onResult) {
        try {
            client.getConnection(connResult -> {
                onResult.call(connResult.succeeded() ? Optional.of(connResult.result()) : Optional.empty());
            });
        } catch (Exception ex) {
            log("Database connection error: " + ex.getLocalizedMessage());
            onResult.call(Optional.empty());
        }
    }

    private void log(final String message) {
        synchronized (System.out) {
            System.out.println("[DatabaseHelper] " + message);
        }
    }
}
