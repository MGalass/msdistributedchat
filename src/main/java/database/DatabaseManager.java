package database;

import database.api.DatabaseApi;
import database.model.Chat;
import database.model.Message;
import database.model.User;
import database.utils.JsonConverter;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sql.SQLClient;
import rx.Subscriber;
import rx.functions.Action1;
import utils.GenericResponse;
import utils.TimeUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * This class serves as a bridge between Vertx and the MySql server (using JDBC)
 */
public class DatabaseManager extends DatabaseHelper implements DatabaseApi {

    private static DatabaseApi instance;

    private DatabaseManager(final SQLClient client) {
        super(client);
    }

    public static DatabaseApi getInstance(final SQLClient client) {
        if (instance == null) {
            instance = new DatabaseManager(client);
        }
        return instance;
    }

    public static void dispose() {
        instance = null;
    }

    //DATABASE

    @Override
    public void createDatabase(String database, Action1<GenericResponse> onResult) {
        String query = "CREATE DATABASE IF NOT EXISTS " + database + ";";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void dropDatabase(String database, Action1<GenericResponse> onResult) {
        String query = "DROP DATABASE " + database + ";";
        log(query);
        executeQuery(query, onResult);
    }

    //USER

    @Override
    public void createUserTable(Action1<GenericResponse> onResult) {
        String query = "CREATE TABLE IF NOT EXISTS USER (username VARCHAR(30) PRIMARY KEY, regdate TIMESTAMP, logged BIT DEFAULT 0);";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void dropUserTable(Action1<GenericResponse> onResult) {
        String query = "DROP TABLE USER;";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void selectUser(String username, Action1<User> onSuccess, Action1<String> onFailure) {
        String query = "SELECT * FROM USER WHERE username = '" + username + "';";
        log(query);
        executeQuery(query,
                jsonArray -> {
                    if (jsonArray.size() > 0) {
                        onSuccess.call(JsonConverter.convertJsonToObject(jsonArray.getJsonObject(0), User.class));
                    } else {
                        onFailure.call("No username found with this username.");
                    }
                },
                onFailure);
    }

    @Override
    public void selectUsers(Action1<List<User>> onSuccess, Action1<String> onFailure) {
        String query = "SELECT * FROM USER;";
        log(query);
        executeQuery(query,
                jsonArray -> onSuccess.call(JsonConverter.convertJsonArrayToList(jsonArray, User.class)),
                onFailure);
    }

    @Override
    public void insertUser(String username, Action1<User> onSuccess, Action1<String> onFailure) {
        String query = "INSERT INTO USER VALUES ('" + username + "','" + TimeUtils.convertDateToTimestamp(Calendar.getInstance()) + "', 0);";
        log(query);
        executeQuery(query,
                jsonArray -> {
                    /* We retrieve the user on insertion */
                    selectUser(username, onSuccess, onFailure);
                },
                onFailure);
    }

    @Override
    public void deleteUser(String username, Action1<GenericResponse> onResult) {
        String query = "DELETE FROM USER WHERE username = '" + username + "';";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void alterUserLoggedStatus(String username, Boolean logged, Action1<GenericResponse> onResult) {
        String query = "UPDATE USER SET logged = " + logged + " WHERE username = '" + username + "';";
        log(query);
        executeQuery(query, onResult);
    }

    //CHAT

    @Override
    public void createChatTable(Action1<GenericResponse> onResult) {
        String query = "CREATE TABLE IF NOT EXISTS CHAT (chatname VARCHAR(30) PRIMARY KEY, createdAt TIMESTAMP);";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void dropChatTable(Action1<GenericResponse> onResult) {
        String query = "DROP TABLE CHAT;";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void selectChat(String chatname, Action1<Chat> onSuccess, Action1<String> onFailure) {
        String query = "SELECT * FROM CHAT WHERE chatname = '" + chatname + "';";
        log(query);
        executeQuery(query,
                jsonArray -> {
                    log(jsonArray.toString());
                    /* We select the users in that chat */
                    selectUsersInChat(chatname, users -> {
                        /* We must convert them to json and add them to the chat */
                        JsonArray userArray = JsonConverter.convertListToJsonArray(users, User.class);
                        JsonObject chatObject = jsonArray.getJsonObject(0);
                        chatObject.put("users", userArray);
                        onSuccess.call(JsonConverter.convertJsonToObject(chatObject, Chat.class));
                    }, onFailure);
                },
                onFailure);
    }

    @Override
    public void selectChats(Action1<List<Chat>> onSuccess, Action1<String> onFailure) {
        String query = "SELECT * FROM CHAT;";
        log(query);
        executeQuery(query,
                jsonArray -> {
                    log(jsonArray.toString());
                    final List<Chat> chats = new ArrayList<>();
                    /* Using a subscriber to aggregate the results */
                    Subscriber<Chat> subscriber = new Subscriber<Chat>() {

                        private int count = 0;

                        @Override
                        public void onCompleted() {
                            onSuccess.call(chats);
                        }

                        @Override
                        public void onError(Throwable e) {
                            onFailure.call(e.getLocalizedMessage());
                        }

                        @Override
                        public void onNext(Chat chat) {
                            chats.add(chat);
                            count++;
                            if (count == jsonArray.size()) {
                                /* We processed all the chats */
                                onCompleted();
                            }
                        }
                    };

                    /* For every chat we have to get the users */
                    for (int i = 0; i < jsonArray.size(); i++) {
                        Chat chat = JsonConverter.convertJsonToObject(jsonArray.getJsonObject(i), Chat.class);
                        /* We need the users too */
                        selectChat(chat.getChatname(),
                                subscriber::onNext,
                                s -> subscriber.onError(new Throwable(s)));
                    }

                    //we must return even when there are no chats
                    if (jsonArray.size() == 0) {
                        subscriber.onCompleted();
                    }
                },
                onFailure);
    }

    @Override
    public void insertChat(String chatname, Action1<Chat> onSuccess, Action1<String> onFailure) {
        String query = "INSERT INTO CHAT VALUES ('" + chatname + "','" + TimeUtils.convertDateToTimestamp(Calendar.getInstance()) + "');";
        log(query);
        executeQuery(query,
                jsonArray -> {
                    /* We retrieve the chat on insertion */
                    selectChat(chatname, onSuccess, onFailure);
                },
                onFailure);
    }

    @Override
    public void deleteChat(String chatname, Action1<GenericResponse> onResult) {
        String query = "DELETE FROM USER WHERE chatname = '" + chatname + "';";
        log(query);
        executeQuery(query, onResult);
    }

    //CHATUSER

    @Override
    public void createUserChatTable(Action1<GenericResponse> onResult) {
        String query = "CREATE TABLE IF NOT EXISTS CHATUSER (chatname VARCHAR(30), username VARCHAR(30),"
                + "FOREIGN KEY (chatname) REFERENCES CHAT(chatname), FOREIGN KEY (username) REFERENCES USER(username), "
                + "PRIMARY KEY (chatname, username));";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void dropUserChatTable(Action1<GenericResponse> onResult) {
        String query = "DROP TABLE CHATUSER;";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void selectUsersInChat(String chatname, Action1<List<User>> onSuccess, Action1<String> onFailure) {
        String query = "SELECT * FROM USER U, CHATUSER CU WHERE U.username = CU.username AND CU.chatname = '" + chatname + "';";
        log(query);
        executeQuery(query,
                jsonArray -> onSuccess.call(JsonConverter.convertJsonArrayToList(jsonArray, User.class)),
                onFailure);
    }

    @Override
    public void insertUserInChat(String username, String chatname, Action1<GenericResponse> onResult) {
        String query = "INSERT INTO CHATUSER VALUES ('" + chatname + "','" + username + "')";
        log(query);
        /* We must check that both exist */
        selectUser(username,
                user -> {
                    /* We found the user */
                    selectChat(chatname,
                            chat -> {
                                /* We found the user too, so we can insert it */
                                executeQuery(query, onResult);
                            },
                            s -> onResult.call(new GenericResponse(false, s)));
                },
                s -> onResult.call(new GenericResponse(false, s)));
    }

    @Override
    public void deleteUserInChat(String username, String chatname, Action1<GenericResponse> onResult) {
        String query = "DELETE FROM CHATUSER WHERE chatname = '" + chatname + "' AND username = '" + username + "';";
        log(query);
        /* We must check that both exist */
        selectUser(username,
                user -> {
                    /* We found the user */
                    selectChat(chatname,
                            chat -> {
                                /* We found the user too, so we can delete it */
                                executeQuery(query, onResult);
                            },
                            s -> onResult.call(new GenericResponse(false, s)));
                },
                s -> onResult.call(new GenericResponse(false, s)));
    }

    //MESSAGE

    @Override
    public void createMessageTable(Action1<GenericResponse> onResult) {
        String query = "CREATE TABLE IF NOT EXISTS CHATMESSAGE (chatname VARCHAR(30), username VARCHAR(30), sentat TIMESTAMP, content VARCHAR(200), "
                + "FOREIGN KEY (chatname) REFERENCES CHAT(chatname), FOREIGN KEY (username) REFERENCES USER(username), "
                + "PRIMARY KEY (chatname, username, sentat));";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void dropMessageTable(Action1<GenericResponse> onResult) {
        String query = "DROP TABLE CHATMESSAGE;";
        log(query);
        executeQuery(query, onResult);
    }

    @Override
    public void selectMessages(String chatname, Action1<List<Message>> onSuccess, Action1<String> onFailure) {
        String query = "SELECT * FROM CHATMESSAGE WHERE chatname = '" + chatname + "';";
        log(query);
        executeQuery(query,
                jsonArray -> onSuccess.call(JsonConverter.convertJsonArrayToList(jsonArray, Message.class)),
                onFailure);
    }

    @Override
    public void selectMessagesFromUser(String chatname, String username, Action1<List<Message>> onSuccess, Action1<String> onFailure) {
        String query = "SELECT * FROM CHATMESSAGE WHERE chatname = '" + chatname + "' AND username = '" + username + "';";
        log(query);
        executeQuery(query,
                jsonArray -> {
                    onSuccess.call(JsonConverter.convertJsonArrayToList(jsonArray, Message.class));
                },
                onFailure);
    }

    @Override
    public void selectMessageFromUser(String chatname, String username, String sentat, Action1<Message> onSuccess, Action1<String> onFailure) {
        String query = "SELECT * FROM CHATMESSAGE WHERE chatname = '" + chatname + "' AND username = '" + username + "' AND sentat = '" + sentat + "';";
        log(query);
        executeQuery(query,
                jsonArray -> {
                    log(jsonArray.encodePrettily());
                    JsonObject messageJson = jsonArray.getJsonObject(0);
                    onSuccess.call(JsonConverter.convertJsonToObject(messageJson, Message.class));
                },
                onFailure);
    }

    @Override
    public void insertMessage(String chatname, String username, String content, Action1<Message> onSuccess, Action1<String> onFailure) {
        String sentat = TimeUtils.convertDateToTimestamp(Calendar.getInstance());
        String query = "INSERT INTO CHATMESSAGE VALUES ('" + chatname + "','" + username + "','" + sentat + "','" + content + "');";
        log(query);
        /* We must check that both chat and username exist */
        selectUser(username,
                user -> {
                    /* We found the user */
                    selectChat(chatname,
                            chat -> {
                                /* We found the user too, so we can insert it */
                                executeQuery(query, insertResult -> {
                                    if (insertResult.getResult()) {
                                        /* We select it and return it */
                                        selectMessageFromUser(chatname, username, sentat, onSuccess, onFailure);
                                    } else {
                                        onFailure.call(insertResult.getMessage());
                                    }
                                });
                            },
                            onFailure);
                },
                onFailure);
    }

    private void log(final String message) {
        synchronized (System.out) {
            System.out.println("[DatabaseManager] " + message);
        }
    }

}
