package database.api;

import database.model.Chat;
import rx.functions.Action1;
import utils.GenericResponse;

import java.util.List;

/**
 * Database api for Chat management
 */
public interface ChatDatabaseApi {

    /**
     * Query: create the chats table
     * @param onResult on result callback
     */
    void createChatTable(Action1<GenericResponse> onResult);

    /**
     * Query: deletes the chats table
     * @param onResult on result callback
     */
    void dropChatTable(Action1<GenericResponse> onResult);

    /**
     * Query: select a chat from the database
     * @param chatname the chatname
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void selectChat(String chatname, Action1<Chat> onSuccess, Action1<String> onFailure);

    /**
     * Query: select all the chats from the database
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void selectChats(Action1<List<Chat>> onSuccess, Action1<String> onFailure);

    /**
     * Query: insert a chat in the database
     * @param chatname the chat name
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void insertChat(String chatname, Action1<Chat> onSuccess, Action1<String> onFailure);

    /**
     * Query: deletes a chat from the database
     * @param chatname the username
     * @param onResult on result callback
     */
    void deleteChat(String chatname, Action1<GenericResponse> onResult);
}
