package database.api;

import database.model.User;
import rx.functions.Action1;
import utils.GenericResponse;

import java.util.List;

/**
 * Database api for ChatUser management
 */
public interface ChatUserDatabaseApi {

    /**
     * Query: create the userchat table
     * @param onResult on result callback
     */
    void createUserChatTable(Action1<GenericResponse> onResult);

    /**
     * Query: deletes the userchat table
     * @param onResult on result callback
     */
    void dropUserChatTable(Action1<GenericResponse> onResult);

    /**
     * Query: select all the users in a chat
     * @param chatname the chat name
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void selectUsersInChat(String chatname, Action1<List<User>> onSuccess, Action1<String> onFailure);

    /**
     * Query: insert a user in a chat
     * @param username the username
     * @param chatname the chat name
     * @param onResult on result callback
     */
    void insertUserInChat(String username, String chatname, Action1<GenericResponse> onResult);

    /**
     * Query: delete a user from a chat
     * @param username the username
     * @param chatname the chat name
     * @param onResult on result callback
     */
    void deleteUserInChat(String username, String chatname, Action1<GenericResponse> onResult);
}
