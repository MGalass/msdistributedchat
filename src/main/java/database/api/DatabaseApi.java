package database.api;

import rx.functions.Action1;
import utils.GenericResponse;

/**
 * DatabaseApi interface, composed by different apis base on different tables/features
 */
public interface DatabaseApi extends UserDatabaseApi, ChatDatabaseApi, ChatUserDatabaseApi, MessageDatabaseApi {

    /**
     * Query: creates a database
     * @param database the database name
     * @param onResult the result callback
     */
    void createDatabase(String database, Action1<GenericResponse> onResult);

    /**
     * Query: drop a database
     * @param database the database name
     * @param onResult the result callback
     */
    void dropDatabase(String database, Action1<GenericResponse> onResult);
}
