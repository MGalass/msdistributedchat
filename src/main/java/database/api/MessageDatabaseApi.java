package database.api;

import database.model.Message;
import rx.functions.Action1;
import utils.GenericResponse;

import java.util.List;

/**
 * Database api for Message management
 */
public interface MessageDatabaseApi {

    /**
     * Query: create the message table
     * @param onResult on result callback
     */
    void createMessageTable(Action1<GenericResponse> onResult);

    /**
     * Query: deletes the message table
     * @param onResult on result callback
     */
    void dropMessageTable(Action1<GenericResponse> onResult);

    /**
     * Query: select all the messages in a chat
     * @param chatname the chat name
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void selectMessages(String chatname, Action1<List<Message>> onSuccess, Action1<String> onFailure);

    /**
     * Query: select all the messages in a chat
     * @param chatname the chat name
     * @param username the username
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void selectMessagesFromUser(String chatname, String username, Action1<List<Message>> onSuccess, Action1<String> onFailure);

    /**
     * Query: select a message in a chat
     * @param chatname the chat name
     * @param username the username
     * @param sentat the timestamp
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void selectMessageFromUser(String chatname, String username, String sentat, Action1<Message> onSuccess, Action1<String> onFailure);

    /**
     * Query: insert a message in a chat
     * @param chatname the chat name
     * @param username the usernmae
     * @param content the message content
     * @param onSuccess on result callback
     * @param onFailure on failure callback
     */
    void insertMessage(String chatname, String username, String content, Action1<Message> onSuccess, Action1<String> onFailure);
}
