package database.api;

import database.model.User;
import rx.functions.Action1;
import utils.GenericResponse;

import java.util.List;

/**
 * Database api for User management
 */
public interface UserDatabaseApi {

    /**
     * Query: create the users table
     * @param onResult on result callback
     */
    void createUserTable(Action1<GenericResponse> onResult);

    /**
     * Query: deletes the users table
     * @param onResult on result callback
     */
    void dropUserTable(Action1<GenericResponse> onResult);

    /**
     * Query: select a user from the database
     * @param username the username
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void selectUser(String username, Action1<User> onSuccess, Action1<String> onFailure);

    /**
     * Query: select all the users from the database
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void selectUsers(Action1<List<User>> onSuccess, Action1<String> onFailure);

    /**
     * Query: insert a user in the database
     * @param username the username
     * @param onSuccess on success callback
     * @param onFailure on failure callback
     */
    void insertUser(String username, Action1<User> onSuccess, Action1<String> onFailure);

    /**
     * Query: deletes a user from the database
     * @param username the username
     * @param onResult on result callback
     */
    void deleteUser(String username, Action1<GenericResponse> onResult);

    /**
     * Query: alter a user in the database
     * @param username the username
     * @param logged the new logged status
     * @param onResult on result callback
     */
    void alterUserLoggedStatus(String username, Boolean logged, Action1<GenericResponse> onResult);
}
