package database.model;

import java.util.List;

/**
 * Model class - Chat
 */
public class Chat {

    private final String chatname;
    private final List<User> users;
    private final String createdAt;

    public Chat(final String chatname, final List<User> users, final String createdAt) {
        this.chatname = chatname;
        this.users = users;
        this.createdAt = createdAt;
    }

    public String getChatname() {
        return chatname;
    }

    public List<User> getUsers() {
        return users;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "[Chat]:  chatname = " + chatname + " - createdAt = " + createdAt
                + users.stream().map(User::toString).reduce("", (s1, s2) -> s1 + "\n" + s2);
    }
}
