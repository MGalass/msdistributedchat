package database.model;

/**
 * Model class - User
 */
public class Message {

    private final String chatname;
    private final String username;
    private final String sentat;
    private final String content;

    public Message(final String chatname, final String username,  final String sentat, final String content) {
        this.chatname = chatname;
        this.username = username;
        this.sentat = sentat;
        this.content = content;
    }

    public String getUsername() {
        return username;
    }

    public String getContent() {
        return content;
    }

    public String getSentat() {
        return sentat;
    }

    @Override
    public String toString() {
        return "[Message]: chatname = " + chatname + " - username = " + username + "\nsentat = " + sentat + " - content = " + content;
    }
}
