package database.model;

/**
 * Model class - User
 */
public class User {

    private final String username;
    private final String regdate;
    private final boolean logged;

    public User(final String username, final String regdate, final boolean logged) {
        this.username = username;
        this.regdate = regdate;
        this.logged = logged;
    }

    public String getUsername() {
        return username;
    }

    public String getRegdate() {
        return regdate;
    }

    public boolean isLogged() {
        return logged;
    }

    @Override
    public String toString() {
        return "[User]: username = " + username + " - regdate = " + regdate + " - logged: " + logged;
    }

}
