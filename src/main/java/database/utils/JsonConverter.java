package database.utils;

import com.google.gson.Gson;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Generic class which converts object to json element and the other way around
 */
public class JsonConverter {

    /**
     * This method converts a JsonObject to the corresponding T object
     * @param jsonObject the JsonObject
     * @param type the class of the Java object
     * @param <T> the generic type
     * @return an object T whose fields are set the same way as the JsonObject
     */
    public static <T> T convertJsonToObject(JsonObject jsonObject, Class<T> type) {
        return new Gson().fromJson(jsonObject.toString(), type);
    }

    /**
     * This method converts a JsonArray to a list of corresponding T object
     * @param jsonArray the JsonArray
     * @param type the class of the Java objects
     * @param <T> the generic type
     * @return an list of T objects whose fields are set the same way as the JsonArray components
     */
    public static <T> List<T> convertJsonArrayToList(JsonArray jsonArray, Class<T> type) {
        List<T> list = new ArrayList<>();
        for (int i = 0; i < jsonArray.size(); i++) {
            list.add(convertJsonToObject(jsonArray.getJsonObject(i), type));
        }
        return list;
    }

    /**
     * This method converts a T object to the corresponding JsonObject
     * @param object the T object
     * @param type the class of the object
     * @param <T> the generic type
     * @return a JsonObject which maps the object fields into key-value pairs
     */
    public static <T> JsonObject convertObjectToJson(T object, Class<T> type) {
        return new JsonObject(new Gson().toJson(object, type));
    }

    /**
     * This method converts a list of T objects to the corresponding JsonArray
     * @param list the list of T objects
     * @param type the class of the objects
     * @param <T> the generic type
     * @return a JsonArray which maps the the list objects into internal JsonObject
     */
    public static <T> JsonArray convertListToJsonArray(List<T> list, Class<T> type) {
        JsonArray jsonArray = new JsonArray();
        list.forEach(obj -> jsonArray.add(convertObjectToJson(obj, type)));
        return jsonArray;
    }
}
