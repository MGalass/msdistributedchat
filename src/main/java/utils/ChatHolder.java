package utils;

import database.model.Chat;
import database.utils.JsonConverter;
import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import verticles.ChatVerticle;
import verticles.DatabaseVerticle;

/*
 * This class holds a chat info and deploys its corresponding ChatVerticle
 */
public class ChatHolder {

    private Chat chatInfo;
    private String eventbusUrl;
    private boolean deployed;
    private ChatVerticle chatService;

    /**
     * Constructor
     * @param chatInfo The chat information
     */
    public ChatHolder(final Chat chatInfo) {
        this.deployed = false;
        this.chatInfo = chatInfo;
        this.eventbusUrl = Constants.BaseEventBusUrl + chatInfo.getChatname();
    }

    /**
     * Gets whether the chat service is deployed or not.
     * @return True, if the service is deployed; otherwise, false.
     */
    public boolean isServiceDeployed() {
        return deployed && this.chatService != null;
    }

    /**
     * Retrieves the verticle representing the service of the chat.
     * @return The chat service verticle
     */
    public ChatVerticle getChatService() {
        return this.chatService;
    }

    /**
     * Gets the chat information.
     * @return The chat information
     */
    public Chat getChatInfo() {
        return this.chatInfo;
    }

    /**
     * Setter: sets new chat information
     * @param chatInfo the new chat information
     */
    public void setChatInfo(final Chat chatInfo) {
        this.chatInfo = chatInfo;
    }

    /**
     * Gets the event bus url to setup a websocket with the chat service verticle.
     * @return The event bus url
     */
    public String getEventbusUrl()  {
        return this.eventbusUrl;
    }
    /**
     * Deploys the chat service verticle
     * @param vertx Vertx system where to deploy the new verticle
     * @param router The main router used by the new instance to mount his own sub router
     * @param callback The callback to track whether the verticle have been successfully deployed
     */
    public void deployService(final Vertx vertx, final Router router, final DatabaseVerticle databaseVerticle, final Handler<AsyncResult<String>> callback) {

        if (isServiceDeployed()) {
            System.out.println("[utils.ChatHolder] Chat service already deployed (id: " + chatInfo.getChatname() + ").");
            return;
        }

        final ChatVerticle chatVerticle = new ChatVerticle(chatInfo.getChatname(), router, databaseVerticle, this.eventbusUrl);
        vertx.deployVerticle(chatVerticle, result -> {
            if (result.succeeded()) {
                this.chatService = chatVerticle;
                this.deployed = true;
                System.out.println("[ChatHolder] Chat service successfully deployed (id: " + chatInfo.getChatname() + ")");
            } else {
                System.out.println("[ChatHolder] Unable to deploy chat service (id: " + chatInfo.getChatname() + "): " + result.cause().getLocalizedMessage());
            }

            callback.handle(result);
        });
    }

    /**
     * Returns the JsonObject that represents this instance.
     * @return The JsonObject that represents this instance
     */
    public JsonObject toJsonObject() {
        return JsonConverter.convertObjectToJson(this.chatInfo, Chat.class).put("eventBusUrl", this.eventbusUrl);
    }
}
