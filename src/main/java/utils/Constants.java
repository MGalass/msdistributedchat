package utils;

public class Constants {

    public static final int ListeningPort = 8080;
    public static final String BaseAPIUrl = "/api/";
    public static final String BaseEventBusUrl = "/eventbus/";
}
