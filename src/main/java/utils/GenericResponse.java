package utils;

/**
 * Utility class which holds a boolean and a String message
 */
public class GenericResponse {

    private final boolean result;
    private final String message;

    public GenericResponse(final boolean result, final String message) {
        this.result = result;
        this.message = message;
    }

    public boolean getResult() {
        return this.result;
    }

    public String getMessage() {
        return message;
    }
}
