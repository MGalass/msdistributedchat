package utils;

/**
 * Utility class which holds a generic T object and a String message
 * @param <T> the type of the object set as the result
 */
public class ObjectResponse<T> {
    private final boolean result;
    private final T object;

    public ObjectResponse(final boolean result, final T object) {
        this.result = result;
        this.object = object;
    }

    public boolean getResult() {
        return this.result;
    }

    public T getObject() {
        return object;
    }
}
