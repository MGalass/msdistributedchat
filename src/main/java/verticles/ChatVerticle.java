package verticles;

import database.model.Message;
import database.model.User;
import database.utils.JsonConverter;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.ErrorHandler;
import io.vertx.ext.web.handler.sockjs.BridgeOptions;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;

import java.util.Optional;

/**
 * A Verticle wich represent a single chat. It gets deployed by the ChatHolder.
 * It has the responsibility to manage clients communication.
 */
@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
public class ChatVerticle extends AbstractVerticle {

    private static final String ADDRESS_OUT = "-messages-out";
    private static final String ADDRESS_OUT_REMOVE_USER = "-users-remove-out";
    private static final String ADDRESS_OUT_INSERT_USER = "-users-insert-out";

    private static final String ADDRESS_IN = "-messages-in";
    private static final String ADDRESS_IN_REMOVE_USER = "-users-remove-in";
    private static final String ADDRESS_IN_INSERT_USER = "-users-insert-in";

    private static final String ENTER_CS_MESSAGE = "enter:cs";
    private static final String EXIT_CS_MESSAGE = "exit:cs";

    private static final long CS_MAX_TIME_MILLIS = 10000;

    private final String chatName;
    private final String eventBusUrl;
    private final DatabaseVerticle databaseVerticle;
    private final Router mainRouter;

    private Optional<String> userInCriticalSection;
    private Optional<Long> csTimerId;

    /**
     * Constructor
     * @param chatName The chat name
     * @param mainRouter The main router where to mount the subrouter created by this verticle
     * @param databaseVerticle The reference to the DatabaseVerticle
     * @param eventBusUrl The binding url of the event bus
     */
    public ChatVerticle(final String chatName, final Router mainRouter, final DatabaseVerticle databaseVerticle, final String eventBusUrl) {
        this.chatName = chatName;
        this.mainRouter = mainRouter;
        this.databaseVerticle = databaseVerticle;
        this.eventBusUrl = eventBusUrl;

        this.userInCriticalSection = Optional.empty();
        this.csTimerId = Optional.empty();
    }

    @Override
    public void start() throws Exception {
        super.start();

        Router router = Router.router(vertx);
        router.route( "/*").handler(eventBusHandler());
        router.route().failureHandler(errorHandler());
        mainRouter.mountSubRouter(this.eventBusUrl, router);

        /* We received a message */
        vertx.eventBus().<String>consumer(chatName + ADDRESS_IN, handler -> {
            JsonObject message = new JsonObject(handler.body());
            String content = message.getString("content");
            /* We won't save empty messages */
            if (content.length() > 0) {
                String chatname = message.getString("chatname");
                String username = message.getString("username");
                log("received: " + message.encodePrettily());

                /* We check if the user can send a message */
                if (!userInCriticalSection.isPresent() || userInCriticalSection.get().equals(username)) {
                    /* The user can proceed */

                    if (content.equals(ENTER_CS_MESSAGE) && (!userInCriticalSection.isPresent() || !userInCriticalSection.get().equals(username))) {
                        /* We received a enter:cs message and the current user in critical section didn't send it */
                        content = "User " + username + " entered in critical section";
                        userInCriticalSection = Optional.of(username);

                        /* We set the timer */
                        csTimerId = Optional.of(vertx.setTimer(CS_MAX_TIME_MILLIS, event -> {
                            /* If the user is still in the criticalSection, we make him exit */
                            if (userInCriticalSection.isPresent()) {
                                userInCriticalSection = Optional.empty();
                                saveAndSendMessage(chatname, username, "User " + username + " exited from critical section");
                                csTimerId = Optional.empty();
                            }
                        }));

                    } else if (content.equals(EXIT_CS_MESSAGE) && userInCriticalSection.isPresent() && userInCriticalSection.get().equals(username)) {
                        /* We received a exit:cs message and the current user in critical section sent it */
                        content = "User " + username + " exited from critical section";
                        userInCriticalSection = Optional.empty();

                        /* If the timer is still on, we have to cancel it */
                        if (csTimerId.isPresent()) {
                            vertx.cancelTimer(csTimerId.get());
                            csTimerId = Optional.empty();
                        }
                    } else if (content.equals(ENTER_CS_MESSAGE) || content.equals(EXIT_CS_MESSAGE)) {
                        /* We choose to discard those reserved messages */
                        return;
                    }

                    /* We insert the message in the chat */
                    saveAndSendMessage(chatname, username, content);

                } else {
                    log("User " + username + " can't send a message because user " + userInCriticalSection.get() + " is in the critical section");
                }
            }
        });

        /* A user exited the chat */
        vertx.eventBus().<String>consumer(chatName + ADDRESS_IN_REMOVE_USER, handler -> {
            JsonObject message = new JsonObject(handler.body());
            String chatname = message.getString("chatname");
            String username = message.getString("username");
            log("received: " + message.encodePrettily());

            /* We get the chat and see if we need to remove the username */
            databaseVerticle.getDatabaseManager().selectUsersInChat(chatname, usersList -> {
                if (usersList.stream().anyMatch(u -> u.getUsername().equals(username))) {
                    /* We found the username*/
                    databaseVerticle.getDatabaseManager().deleteUserInChat(username, chatname, resultMessage -> {
                        /* We successfully deleted the user from chat, so we have to let the others know */
                        if (resultMessage.getResult()) {
                            /* We retrieve the chat and the other users */
                            selectUsersAndSendMessage(chatname, username, chatName + ADDRESS_OUT_REMOVE_USER);
                        } else {
                            sendMessage(resultMessage.getMessage(), chatName + ADDRESS_OUT_REMOVE_USER);
                        }
                    });
                } else {
                    /* We didn't find the username, so we don't have to remove him/her*/
                    log("User" + username + "not found in chat " + chatname + ", so we didn't remove it");
                    selectUsersAndSendMessage(chatname, username,chatName + ADDRESS_OUT_REMOVE_USER);
                }

            }, failure -> sendMessage(failure, chatName + ADDRESS_OUT_REMOVE_USER));
        });

        /* A user entered the chat */
        vertx.eventBus().<String>consumer(chatName + ADDRESS_IN_INSERT_USER, handler -> {
            JsonObject message = new JsonObject(handler.body());
            String chatname = message.getString("chatname");
            String username = message.getString("username");
            log("received: " + message.encodePrettily());

            /* We get the chat and check if the user is in it */
            databaseVerticle.getDatabaseManager().selectUsersInChat(chatname, usersList -> {
                if (usersList.stream().noneMatch(u -> u.getUsername().equals(username))) {
                    /* The user is not present, so we need to insert him */
                    databaseVerticle.getDatabaseManager().insertUserInChat(username, chatname, resultMessage -> {
                        /* We successfully inserted the user in the chat chat, so we have to let the others know */
                        if (resultMessage.getResult()) {
                            /* We retrieve the chat and the other users */
                            selectUsersAndSendMessage(chatname, username, chatName + ADDRESS_OUT_INSERT_USER);
                        } else {
                            sendMessage(resultMessage.getMessage(), chatName + ADDRESS_OUT_INSERT_USER);
                        }
                    });
                } else {
                    /* The user is present, we do nothing and send the message */
                    log("User" + username + "not found in chat " + chatname + ", so we didn't insert it");
                    selectUsersAndSendMessage(chatname, username, chatName + ADDRESS_OUT_INSERT_USER);
                }
            }, failure -> sendMessage(failure, chatName + ADDRESS_OUT_INSERT_USER));
        });

    }

    /**
     * This method saves a message in the database and send it though the EventBus on success
     * @param chatname the chat name
     * @param username the sender
     * @param content the message content
     */
    private void saveAndSendMessage(final String chatname, final String username, final String content) {
        databaseVerticle.getDatabaseManager().insertMessage(chatname, username, content, resultMessage -> {
            /* We successfully inserted the message, so we send it back*/
            sendMessage(JsonConverter.convertObjectToJson(resultMessage, Message.class).encode(), chatName + ADDRESS_OUT);
        }, this::log);
    }

    /**
     * This method retrieves the chat users and send the result as a message through the EventBus
     * @param chatname the chat name
     * @param username the user that made the request
     * @param address the EventBus address we want to send the message through
     */
    private void selectUsersAndSendMessage(final String chatname, final String username, final String address) {
        databaseVerticle.getDatabaseManager().selectUsersInChat(chatname, otherUsers -> {
            JsonArray users = JsonConverter.convertListToJsonArray(otherUsers, User.class);
            JsonObject json = new JsonObject();
            json.put("chatname", chatname);
            json.put("username", username);
            json.put("chatusers", users);
            sendMessage(json.encode(), address);

        }, failure -> sendMessage(failure, address));
    }

    /**
     * The socket handler
     * @return The SockJsHandler
     */
    private SockJSHandler eventBusHandler() {
        BridgeOptions options = new BridgeOptions()
                .addOutboundPermitted(new PermittedOptions().setAddressRegex(chatName + ADDRESS_OUT))
                .addOutboundPermitted(new PermittedOptions().setAddressRegex(chatName + ADDRESS_OUT_REMOVE_USER))
                .addOutboundPermitted(new PermittedOptions().setAddressRegex(chatName + ADDRESS_OUT_INSERT_USER))
                .addInboundPermitted(new PermittedOptions().setAddressRegex(chatName + ADDRESS_IN))
                .addInboundPermitted(new PermittedOptions().setAddressRegex(chatName + ADDRESS_IN_REMOVE_USER))
                .addInboundPermitted(new PermittedOptions().setAddressRegex(chatName + ADDRESS_IN_INSERT_USER));
        return SockJSHandler.create(vertx).bridge(options);
    }

    /**
     * The error handler
     * @return The error handler
     */
    private ErrorHandler errorHandler() {
        return ErrorHandler.create();
    }

    /**
     * Sends a message to the event bus.
     * @param message The message
     */
    public void sendMessage(final String message, final String address) {
        log("sent message: " + message + "on address: " + address);
        vertx.eventBus().publish(address, message);
    }

    /**
     * Prints a message to the console.
     * @param message The message
     */
    private void log(final String message) {
        synchronized (System.out) {
            System.out.println("[ChatVerticle] " + message);
        }
    }
}
