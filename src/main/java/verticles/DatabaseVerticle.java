package verticles;

import com.mysql.cj.util.StringUtils;
import database.DatabaseManager;
import database.api.DatabaseApi;
import utils.GenericResponse;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLClient;
import rx.functions.Action1;

/**
 * A Verticle who manages the database connections and serves as the bridge between the actual database and the rest of the software
 */
public class DatabaseVerticle extends AbstractVerticle {

    private final String databaseName;
    private final String user;
    private final String password;

    private SQLClient client;

    public DatabaseVerticle(final String databaseName, final String user, final String password) {
        this.databaseName = databaseName;
        this.user = user;
        this.password = password;
    }

    /**
     * Getter. This method return the database name
     * @return the database name
     */
    private String getDatabaseName() {
        return databaseName;
    }

    /**
     * This method retrieves the DatabaseManager singleton instance.
     * @return a DatabaseApi implementation made by DatabaseManager
     */
    DatabaseApi getDatabaseManager() {
        return DatabaseManager.getInstance(client);
    }

    /**
     * This method initializes the database
     * @param callback the response callback
     */
    void initDatabase(Action1<GenericResponse> callback) {
        getDatabaseManager().createDatabase(getDatabaseName(), db -> {
            if (db.getResult()) {
                /* init jdbc client to select the database by default */
                client.close(event -> {
                    if (event.succeeded()) {
                        createTables(callback);
                    } else {
                        callback.call(new GenericResponse(false, "Error: re-establish connection to select " +  getDatabaseName() + " database."));
                    }
                });
            } else {
                callback.call(new GenericResponse(false, "Error: database creation failed"));
            }
        });
    }

    /**
     * This method creates all the database tables
     * @param callback the response callback
     */
    private void createTables(Action1<GenericResponse> callback) {
        DatabaseManager.dispose();
        client = getJDBCClient(getDatabaseName());
        getDatabaseManager().createChatTable(chatTable -> {
            if (chatTable.getResult()) {
                getDatabaseManager().createUserTable(userTable -> {
                    if (userTable.getResult()) {
                        getDatabaseManager().createUserChatTable(userChatTable -> {
                            if (userChatTable.getResult()) {
                                getDatabaseManager().createMessageTable(messageTable -> {
                                    if (messageTable.getResult()) {
                                        callback.call(new GenericResponse(true, null));
                                    } else {
                                        callback.call(new GenericResponse(false, "Error: message table creation failed"));
                                    }
                                });
                            } else {
                                callback.call(new GenericResponse(false, "Error: user chat table creation failed"));
                            }
                        });
                    } else {
                        callback.call(new GenericResponse(false, "Error: user table creation failed"));
                    }
                });
            } else {
                callback.call(new GenericResponse(false, "Error: chat table creation failed"));
            }
        });
    }

    @Override
    public void start() throws Exception {
        super.start();

        /* Init jdbc without a pre-selected database */
        client = getJDBCClient(null);
    }

    /**
     * This method returns a basic JDBClient
     * @return a SQLClient
     */
    private SQLClient getJDBCClient(final String databaseName) {
        JsonObject config = new JsonObject()
                .put("url", "jdbc:mysql://localhost/" + (StringUtils.isNullOrEmpty(databaseName) ? "" : databaseName) +
                        "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC")
                .put("driver_class", "com.mysql.cj.jdbc.Driver")
                .put("max_pool_size", 60)
                .put("user", user)
                .put("password", password);

        return JDBCClient.createShared(vertx, config);
    }

    private void log(final String message) {
        synchronized (System.out) {
            System.out.println("[DatabaseVerticle] " + message);
        }
    }
}
