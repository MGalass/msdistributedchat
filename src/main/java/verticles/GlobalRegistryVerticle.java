package verticles;

import com.mysql.cj.util.StringUtils;
import database.api.DatabaseApi;
import database.model.Chat;
import database.model.User;
import database.utils.JsonConverter;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.StaticHandler;
import rx.functions.Action1;
import utils.ChatHolder;
import utils.Constants;
import utils.GenericResponse;
import utils.ObjectResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * A Verticle which is the main api handler. It can deploy other Verticle, as such as the ChatVerticle.
 */
public class GlobalRegistryVerticle extends AbstractVerticle {

    /** REST API ROUTES DEFINITIONS
     * /app - let client to retrieve static resources for the web-app
     * /api/login - POST used to authenticate the client and make it active
     * /api/logout - POST used to make the client to logout from the system and make it inactive
     * /api/chats - GET retrieve the list of chats
     * /api/chat:chatID - GET - retrieve information about a chat
     * /api/chat - POST - creates a new chat
     * /api/clients - GET - retrieve the list of clients
     * /api/client:clientID - GET - retrieve information about a client
     * /api/eventbus/chatID/ - WS - websocket to let clients to exchange data with the chat verticle
     */

    //region Verticle fields

    private Router router;
    private DatabaseVerticle databaseVerticle;
    private Map<String, ChatHolder> activeChats;

    //endregion

    //region Verticle main methods

    /**
     * Constructor
     * @param databaseVerticle The database verticle
     */
    public GlobalRegistryVerticle(final DatabaseVerticle databaseVerticle) {
        this.databaseVerticle = databaseVerticle;
        this.activeChats = new HashMap<>();
    }

	@Override
	public void start() throws Exception {
        super.start();

        setup(res ->  {
            if (res.getResult()) {
                log("Database setup completed");

                router = Router.router(vertx);

                router.route().handler(BodyHandler.create());
                router.route().handler(CookieHandler.create());

                router.route().failureHandler(ctx -> ctx.response().setStatusCode(404).end());

                router.get("/app*").handler(StaticHandler.create());

                router.post(Constants.BaseAPIUrl + "login").handler(this::handleLogin);
                router.post(Constants.BaseAPIUrl + "logout").handler(this::handleLogout);

                router.get(Constants.BaseAPIUrl + "chats").handler(this::handleGetChats);
                router.get(Constants.BaseAPIUrl + "chat/:chatID").handler(this::handleGetChat);
                router.post(Constants.BaseAPIUrl + "chat/:chatID").handler(this::handleCreateChat);

                router.get(Constants.BaseAPIUrl + "clients").handler(this::handleGetClients);
                router.get(Constants.BaseAPIUrl + "client/:clientID").handler(this::handleGetClient);

                vertx.createHttpServer().requestHandler(router::accept).listen(Constants.ListeningPort);

                log("Service ready, listening on: http://localhost:" + Constants.ListeningPort + "/app");
            } else {
                log("Database setup error: " + res.getMessage());
            }
        });
	}

	//endregion

    //region REST API handlers
    private void handleGetClient(RoutingContext routingContext) {
        log("get client");

        HttpServerResponse response = routingContext.response();

        /* Getting the name from the api parameter */
        String username = routingContext.request().getParam("clientID");

        getDb().selectUser(username, user -> {
            /* We retrieved the user */
            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setStatusCode(200)
                    .end(new JsonObject().put("status", 0).put("message", "ok").put("data", JsonConverter.convertObjectToJson(user, User.class)).encode());
        }, error -> {
            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setStatusCode(200)
                    .end(new JsonObject().put("status", 1).put("message", error).encode());
        });
    }

    private void handleGetClients(RoutingContext routingContext) {
        log("get clients");

        HttpServerResponse response = routingContext.response();

        getDb().selectUsers(users -> {
            /* We retrieved the users */
            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setStatusCode(200)
                    .end(new JsonObject().put("status", 0).put("message", "ok").put("data", JsonConverter.convertListToJsonArray(users, User.class)).encode());
        }, error -> {
            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setStatusCode(200)
                    .end(new JsonObject().put("status", 1).put("message", error).encode());
        });
    }

    private void handleCreateChat(RoutingContext routingContext) {
        log("create chat");

        HttpServerResponse response = routingContext.response();

        String chatname = routingContext.request().getParam("chatID");

        if (StringUtils.isNullOrEmpty(chatname)) {
            sendError(response, 400, "Invalid chatname");
        } else {
            getDb().insertChat(chatname, result -> {
                response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                        .setStatusCode(200)
                        .end(new JsonObject().put("status", 0).put("message", "ok").put("data", JsonConverter.convertObjectToJson(result, Chat.class)).encode());
            }, error -> {
                response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                        .setStatusCode(200)
                        .end(new JsonObject().put("status", 1).put("message", error).encode());
            });
        }

    }

    private void handleGetChat(RoutingContext routingContext) {
        log("get chat");

        HttpServerResponse response = routingContext.response();

        String chatName = routingContext.request().getParam("chatID");
        String username = routingContext.request().getParam("username");

        if (!StringUtils.isNullOrEmpty(chatName)) {
            getDb().selectChat(chatName, selectedChat -> {

                /* We must insert the user in the chat */
                if (username != null) {
                    getDb().insertUserInChat(username, chatName, userInChatRes -> {
                        if (userInChatRes.getResult()) {

                            /* We inserted the user, so we get the chat again */
                            getDb().selectChat(chatName, updatedChat -> {
                                initChatService(selectedChat, result -> {
                                    if (result.getResult()) {
                                        response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                                .setStatusCode(200)
                                                .end(new JsonObject().put("status", 0).put("message", "ok").put("data", result.getObject().toJsonObject()).encode());
                                    } else {
                                        response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                                .setStatusCode(200)
                                                .end(new JsonObject().put("status", 1).put("message", "Unable to initialize chat service").encode());
                                    }
                                });
                            }, failure -> {
                                response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                        .setStatusCode(200)
                                        .end(new JsonObject().put("status", 1).put("message", failure).encode());
                            });
                        } else {
                            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                    .setStatusCode(200)
                                    .end(new JsonObject().put("status", 1).put("message", userInChatRes.getMessage()).encode());
                        }
                    });
                }
            }, failure -> {
                response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                        .setStatusCode(200)
                        .end(new JsonObject().put("status", 1).put("message", failure).encode());
            });
        } else {
            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setStatusCode(200)
                    .end(new JsonObject().put("status", 1).put("message", "Chat name is not valid.").encode());
        }
    }

    private void handleGetChats(RoutingContext routingContext) {
        log("get chats");

        HttpServerResponse response = routingContext.response();
        getDb().selectChats(success -> {
            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setStatusCode(200)
                    .end(new JsonObject().put("status", 0).put("message", "ok").put("data", JsonConverter.convertListToJsonArray(success, Chat.class)).encode());
        }, failure -> {
            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                    .setStatusCode(200)
                    .end(new JsonObject().put("status", 1).put("message", failure).encode());
        });
    }

    private void handleLogin(RoutingContext routingContext) {
        log("login");

        HttpServerResponse response = routingContext.response();
        String username = routingContext.request().getFormAttribute("username");

        if (StringUtils.isNullOrEmpty(username)) {
            sendError(response, 400, "Invalid username");
        } else {

            getDb().selectUser(username, onSuccess -> {
                if (!onSuccess.isLogged()) {
                    getDb().alterUserLoggedStatus(username, true, onResult -> {
                        if (onResult.getResult()) {
                            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                    .setStatusCode(200)
                                    .end(new JsonObject().put("status", 0).put("message", "ok").encode());
                        } else {
                            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                    .setStatusCode(200)
                                    .end(new JsonObject().put("status", 1).put("message", "Another client with the same username is logged.").encode());
                        }
                    });
                } else {
                    response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .setStatusCode(200)
                            .end(new JsonObject().put("status", 1).put("message", "Another client with the same username is logged.").encode());
                }
            }, onFailed -> {
                getDb().insertUser(username, result -> {
                    /* We inserted the user and now we set it to logged */
                    getDb().alterUserLoggedStatus(username, true, onResult -> {
                        if (onResult.getResult()) {
                            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                    .setStatusCode(200)
                                    .end(new JsonObject().put("status", 0).put("message", "ok").encode());
                        } else {
                            response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                    .setStatusCode(200)
                                    .end(new JsonObject().put("status", 1).put("message", onResult.getMessage()).encode());
                        }
                    });
                }, error -> {
                    response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .setStatusCode(200)
                            .end(new JsonObject().put("status", 1).put("message", error).encode());
                });
            });
        }
    }

    private void handleLogout(RoutingContext routingContext) {
        log("logout");

        HttpServerResponse response = routingContext.response();

        String username = routingContext.request().getFormAttribute("username");
        String chatname = routingContext.request().getFormAttribute("chatID");

        if (StringUtils.isNullOrEmpty(username)) {
            sendError(response, 400, "Invalid username");
        } else {
            /* We set the user as logged */
            getDb().alterUserLoggedStatus(username, false, result -> {
                if (result.getResult()) {
                    /* We delete the user from the chat if he was in any*/
                    if (!StringUtils.isNullOrEmpty(chatname)) {
                        getDb().deleteUserInChat(username, chatname, delUserChatRes -> {
                            if (!delUserChatRes.getResult()) {
                                response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                                        .setStatusCode(200)
                                        .end(new JsonObject().put("status", 1).put("message", result.getMessage()).encode());
                            }
                        });
                    }

                    /* We succeeded the logout */
                    response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .setStatusCode(200)
                            .end(new JsonObject().put("status", 0).put("message", "ok").encode());

                } else {
                    response.putHeader(HttpHeaders.CONTENT_TYPE, "application/json")
                            .setStatusCode(200)
                            .end(new JsonObject().put("status", 1).put("message", result.getMessage()).encode());
                }
            });
        }
    }
    //endregion

    //region Helper methods
    /**
     * Initialize the database verticle asynchronously.
     * @param onResult The callback to handle database initialization completion
     */
    private void setup(Action1<GenericResponse> onResult) {
        databaseVerticle.initDatabase(onResult);
    }

    /**
     * Gets the database manager.
     * @return The database manager
     */
    private DatabaseApi getDb() {
        if (databaseVerticle == null) {
            throw new IllegalStateException("Database verticle is null!");
        }

        return databaseVerticle.getDatabaseManager();
    }

    /**
     * Sends an error.
     * @param response The HttpServeResponse where to send the error.
     * @param statusCode The error code.
     * @param message The message of the error.
     */
    private void sendError(final HttpServerResponse response, final int statusCode, final String message) {
        JsonObject jsonObject = new JsonObject();
	    if (StringUtils.isNullOrEmpty(message)) {
            jsonObject.put("status", -2).put("message", message);
        } else {
            jsonObject.put("status", -1).put("message", "generic error");
        }
        response.setStatusCode(statusCode).end(jsonObject.encode());
    }

    /**
     * Initialize the service verticle of a specific chat if it wasn't already deployed.
     * @param chat The chat
     * @param handler The completion handler
     */
    private void initChatService(final Chat chat, final Action1<ObjectResponse<ChatHolder>> handler) {
        ChatHolder chatHolder = this.activeChats.get(chat.getChatname());

        if (chatHolder == null) {
            chatHolder = new ChatHolder(chat);
            activeChats.put(chat.getChatname(), chatHolder);
        } else {
            /* We update with the new users */
            chatHolder.setChatInfo(chat);
        }

        if (!chatHolder.isServiceDeployed()) {
            final ChatHolder responseObj = chatHolder;
            /* We deploy the ChatVerticle */
            chatHolder.deployService(vertx, router, databaseVerticle, result -> {
                if (result.succeeded()) {
                    handler.call(new ObjectResponse<>(true, responseObj));
                } else {
                    handler.call(new ObjectResponse<>(false, null));
                }
            });
        } else {
            /* We already deployed the ChatVerticle */
            handler.call(new ObjectResponse<>(true, chatHolder));
        }
    }

    /**
     * Prints a message to the console.
     * @param message The message to print
     */
	private void log(final String message) {
	    synchronized (System.out) {
	        System.out.println("[GlobalRegistryVerticle] " + message);
        }
    }
    //endregion
}