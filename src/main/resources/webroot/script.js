var base_url = 'http://localhost:8080';
var api_url = base_url + '/api/';
//event bus base addresses
var addresses = {
    Users: {
        Insert: {
            In: "-users-insert-in",
            Out: "-users-insert-out"
        },
        Remove: {
            In: "-users-remove-in",
            Out: "-users-remove-out"
        }
    },
    Messages: {
        In: "-messages-in",
        Out: "-messages-out"
    }
};

//current user state info
var session = { username: "", chatID: "" };
var eventBus = null;

moment.locale('en');

$(document).ready(function(){

    $('.user-info-container').hide();
    $('.create-chat-container').hide();
    $('.chat-list-container').hide();
    $('.chat-container').hide();

    $('#username').on('keypress', function (e) {
         if (e.which === 13) {
            $(this).attr("disabled", "disabled");
            $("#login-button").click();
            $(this).removeAttr("disabled");
         }
     });

     /* We show the existing users */
    renderClients();

    $("#login-button").on('click', function() {
        $('.error').remove();

        var username = $('#username').val();
        if (username && username.length > 0) {
            checkLogin(username);
        } else {
            $('.login-container').append('<div class="error">You must specify a username.</div>');
        }
    });

    $(window).on("unload", function(e) {
        logout();
    });

    /* We retrieve as a test the clients info */
    getClients();
});

var checkLogin = function(username) {
    var $container = $('.login-container');

    $.post( api_url + "login", { 'username': username }, function(data) {
        if (data) {
            if (data.status == 0) {
                session.username = username;

                $container.remove();

                /* We remove the other clients names */
                $('.clients_container').hide();

                /* Retrieving user info */
                getClient(username);

                /* We did the login */
                renderUserInfo();
                renderChatCreation();
                renderChats();
            } else {
                $container.append('<div class="error">' + data.message + '</div>');
            }
        } else {
            $container.append('<div class="error">Empty server reply.</div>');
        }
    }, 'json');
};

var openChat = function(e) {
    var $self = $(this);
    var chatName = $self.attr("data-chatname").trim();

    /* If we are already in a chat, we have to exit it */
    if (eventBus != null) {
        console.log("Deleting user " + session.username + " from chat " + session.chatID);

        var jsonDeleteUserFromChat = {
            "chatname" : session.chatID,
            "username" : session.username,
        };

        /* We tell the others that we exited the chat */
        eventBus.send(session.chatID + addresses.Users.Remove.In, JSON.stringify(jsonDeleteUserFromChat));

        /* Resetting current chat and eventBus */
        session.chatID = "";

        console.log("Closing eventBus: " + eventBus);
        eventBus.close();
        eventBus = null;

    }

    /* We make a get request for the selected chat. We use the username to insert it in the database */
    $.get( api_url + "chat/" + chatName, {'username' : session.username}, function(chat) {

        if (chat && chat.data) {
            console.log('bus: ' + base_url + chat.data.eventBusUrl);

            $('.chat-messages-container').empty();

            var options = {
                vertxbus_reconnect_attempts_max: Infinity, // Max reconnect attempts
                vertxbus_reconnect_delay_min: 1000, // Initial delay (in ms) before first reconnect attempt
                vertxbus_reconnect_delay_max: 5000, // Max delay (in ms) between reconnect attempts
                vertxbus_reconnect_exponent: 2, // Exponential backoff factor
                vertxbus_randomization_factor: 0.5 // Randomization factor between 0 and 1
            };

            /* We show the users */
            renderChatUsers(chat.data.users)

            //event bus initialization
            eventBus = new EventBus(base_url + chat.data.eventBusUrl, options);
            eventBus.enableReconnect = true;

            eventBus.onreconnect = function() {
                console.log('Socket: onreconnect');
            }
            eventBus.onopen = function() {
                 console.log('Socket: connected');

                 $('#send-button').prop("disabled", false);
                 $('#messateText').prop('disabled', false);

                 /* Setting the new chat */
                 session.chatID = chatName;

                 /* We entered the chat, so we have to tell the others */
                var jsonInsertUserInChat = {
                    "chatname" : session.chatID,
                    "username" : session.username,
                };

                /* We tell the others that we exited the chat */
                eventBus.send(session.chatID + addresses.Users.Insert.In, JSON.stringify(jsonInsertUserInChat));

                //Receive message
                eventBus.registerHandler(session.chatID + addresses.Messages.Out, function(error, message) {
                    console.log('Received: ' + message.body);

                    $('.error').remove();

                    var data = JSON.parse(message.body);
                    var sender = data.username;
                    var content = data.content;
                    var date = moment(data.sentat.replace('Z', ''));
                    var humanizedDate = date.from(moment());
                    var isMine = (sender == session.username);

                    $('.chat-messages-container').append('<div class="' + (isMine ? 'container darker' : 'container') + '">' +
                                                            '<div class="circle" alt="Avatar">' + (isMine ? 'Me': sender) + '</div>' +
                                                            '<p title="' + content + '">' + content + '</p>' +
                                                            '<span class="time-right">' + date.format('DD/MM/YYYY hh:mm') + '</span>' +
                                                         '</div>');

                    $('.chat-messages-container').stop().animate({
                        scrollTop: $('.chat-messages-container')[0].scrollHeight
                    }, 800);

                    $('#message-text').focus();
                });

                /* A user left the chat */
                eventBus.registerHandler(session.chatID + addresses.Users.Remove.Out, function(error, message) {
                    console.log('Received: ' + message.body);

                    $('.error').remove();

                    var data = JSON.parse(message.body);
                    var sender = data.username;
                    var chatname = data.chatname;
                    var chatusers = data.chatusers;
                    var isMine = (sender == session.username);

                    $('.chat-messages-container').append('<div class="' + (isMine ? 'container darker' : 'container') + '">' +
                                                            '<div class="circle" alt="Avatar">' + (isMine ? 'Me': sender) + '</div>' +
                                                            '<p>Exited the chat ' + chatname + '</p>' +
                                                         '</div>');

                    $('#message-text').focus();

                    /* We update the chats */
                    renderChats();

                    /* We update the users */
                    renderChatUsers(chatusers);
                });

                /* A user entered the chat */
                eventBus.registerHandler(session.chatID + addresses.Users.Insert.Out, function(error, message) {
                    console.log('Received: ' + message.body);

                    $('.error').remove();

                    var data = JSON.parse(message.body);
                    var sender = data.username;
                    var chatname = data.chatname;
                    var chatusers = data.chatusers;
                    var isMine = (sender == session.username);

                    $('.chat-messages-container').append('<div class="' + (isMine ? 'container darker' : 'container') + '">' +
                                                            '<div class="circle" alt="Avatar">' + (isMine ? 'Me': sender) + '</div>' +
                                                            '<p>Entered the chat ' + chatname + '</p>' +
                                                         '</div>');

                    $('#message-text').focus();

                    /* We update the chats */
                    renderChats();

                    /* We update the users */
                    renderChatUsers(chatusers);
                });
            }
            eventBus.onclose = function() {
                console.log("Socket: disconnected");
                eventBus = null;

                $('#send-button').prop("disabled", true);
                $('#messateText').prop('disabled', true);
            };

            //add event listeners for input controls
            $('#send-button').on('click', function () {
                var messageText = $('#message-text').val();
                var jsonMessage = {
                    'chatname' : session.chatID,
                    'username' : session.username,
                    'content' : messageText
                };
                if (eventBus) {
                    eventBus.send(session.chatID + addresses.Messages.In, JSON.stringify(jsonMessage));
                }
                $('#message-text').val('');
            });
            $('#message-text').on('keypress', function (e) {
                 if (e.which === 13) {
                    $(this).attr("disabled", "disabled");
                    $("#send-button").click();
                    $(this).removeAttr("disabled");
                 }
            });

            $('.chat-name').text('Chat name: ' + chatName);
            $('.chat-name').show();
        } else {
            console.log("invalid chat data received: " + JSON.stringify(chat));

            $('.chat-container').append('<div class="error">' + chat.message + '</div>');
        }

        $('.chat-container').show();
    });
};

var createChat = function(e) {
    var $container = $('.create-chat-container');
    var newChatName = $('#new-chat-name').val();

    $('.error').remove();

    if (newChatName && newChatName.length > 0) {
       $.post( api_url + "chat/" + newChatName, function(result) {
             if (result) {
                 if (result.status == 0 && result.data) {
                    $('#new-chat-name').val("");
                    $('.error').remove();
                    renderChats();
                 } else {
                    $container.append('<div class="error">' + result.message + '</div>');
                 }
             } else {
                $container.append('<div class="error">Empty response from server.</div>');
             }
       });
    } else {
        $container.append('<div class="error">Invalid chat name.</div>');
    }
};

var renderChats = function () {
    var $container = $('.chat-list-container');
    $container.empty();
    $('.chat-list-container').append('<h3>Available chats</h3>');

    $.get(api_url + "chats", function(chats) {
        var $html = $('<table id="chat-table" class="table table-striped"><tr><th scope="col">Name</th><th scope="col">Members</th><th scope="col">Creation date</th></tr></table>');

         if (chats && chats.data) {
            $.each(chats.data, function (key, value) {
                var dateTime = new Date(value.createdAt);
                dateTime = moment(dateTime).format("DD-MM-YYYY HH:mm:ss");
                $html.append('<tr><td><a data-chatname="' + value.chatname + '">' + value.chatname + '</a></td><td>' + value.users.length + '</td><td>' + dateTime + '</td></tr>');
            });
         }

        $container.append($html);
        $container.show();

        $("#chat-table").on('click', 'a', openChat);
    });
};

var renderChatUsers = function (chatusers) {
    var $container = $('.chat-users');

    $container.empty();
    $container.append('<h3>Users in chat: </h3>');

    $.each(chatusers, function (key, value) {
        if (value.logged) {
            /* If the user is logged */
            $container.append('<h6 style="color:#0000ff">' + value.username + '</h6>');
        } else {
           $container.append('<h6>' + value.username + '</h6>');
        }
    });

    $container.show();
};

var renderUserInfo = function () {
    var $container = $(".user-info-container");

    $container.empty();
    $container.append('<h2>User info</h2><p>You\'re now logged as: ' + session.username + '</p><button id="logout-button" class="btn btn-danger">Logout</button>');
    $container.show();

    $("#logout-button").on('click', logout);
};

var renderChatCreation = function () {
    $('.create-chat-container').show();

    $('#new-chat-name').on('keypress', function (e) {
        if (e.which === 13) {
            $(this).attr("disabled", "disabled");
            $("#create-chat-button").click();
            $(this).removeAttr("disabled");
        }
    });

    $("#create-chat-button").on('click', createChat);
};

var renderClients = function () {
     var $container = $('.clients_container');

     $container.empty();
     var userNames = 'Existing clients: ';
     /* We get the clients */
     $.get( api_url + "clients/", function(users) {
        console.log('Clients info: ' + JSON.stringify(users));
        $.each(users.data, function (key, value) {
            userNames += value.username + ', ';
        });
        userNames = userNames.substring(0, userNames.length - 2);
        $container.html('<h6>' + userNames + '</h6>');
        $container.show();
     });
};

var logout = function () {
    console.log("Logout: " + session.username);
    if (session.chatID != "") {
        console.log("Exit from chat: " + session.chatID);
    }
    $.post( api_url + "logout", { 'username' : session.username, 'chatID' : session.chatID}, function(data) {
        console.log('logout: ' + data);
        window.location = '/app/index.html';
    }, 'json');
};

var getClient = function (username) {
    $.get( api_url + "client/" + username, function(user) {
        console.log('Client info: ' + JSON.stringify(user));
    });
};

var getClients = function () {
    $.get( api_url + "clients/", function(users) {
        console.log('Clients info: ' + JSON.stringify(users));
    });
};